###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. 											##
##                                                                                          ##  
##                           AITAIT - Gas analyser						                        ##
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0,1.1             ####################################
# Author:  Andre Bengtsson
# Date:    18-04-23
# Version: v1.0



############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",   PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",           PV_DESC="Operation Mode Manual", PV_ONAM="True",           PV_ZNAM="False")

#pump states
add_digital("D_Running",  ARCHIVE=True,  PV_DESC="Diaphragm Pump Running",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("D_Stopped",  ARCHIVE=True,  PV_DESC="Diaphragm Pump Stopped",          PV_ONAM="True",           PV_ZNAM="False")
add_analog("D_PumpColor", "INT",         PV_DESC="Diaphragm BlockIcon pump color")

add_digital("T_Running",  ARCHIVE=True,  PV_DESC="Turbo Pump Running",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("T_Stopped",  ARCHIVE=True,  PV_DESC="Turbo Pump Stopped",          PV_ONAM="True",           PV_ZNAM="False")
add_analog("T_PumpColor", "INT",         PV_DESC="Diaphragm BlockIcon pump color")

add_digital("Running",  ARCHIVE=True,  PV_DESC="Running",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("Stopped",  ARCHIVE=True,  PV_DESC="Stopped",          PV_ONAM="True",           PV_ZNAM="False")
add_analog("AnalyserColor", "INT",         PV_DESC="Gasanalyser BlockIcon color")

add_digital("Emission",  ARCHIVE=True,  PV_DESC="Emission started",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("EM",  ARCHIVE=True,  PV_DESC="Electron Multiplier started",          PV_ONAM="True",           PV_ZNAM="False")

add_analog("Pressure","REAL" ,                     PV_DESC="Pressure in RGA",                PV_EGU="mbar")
add_digital("VacEstablished",  PV_DESC="Vacuum established",          PV_ONAM="True",           PV_ZNAM="False")

add_digital("V1",  PV_DESC="Valve1 open",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("V2",  PV_DESC="Valve1 open",          PV_ONAM="True",           PV_ZNAM="False")
add_digital("Vvalve",  PV_DESC="venting valve open",          PV_ONAM="True",           PV_ZNAM="False")
add_analog("CapilHeat", "INT",         PV_DESC="capillary heat status")
add_analog("InlHeat", "INT",         PV_DESC="inlet heat")
add_analog("VentDelay", "INT",         PV_DESC="Venting delay time")
add_analog("AnalysDelay", "INT",         PV_DESC="Delay time before PPM alarm trigger")

add_analog("IonCurrent_H2","REAL" ,                     PV_DESC="Hydrogen ion current",                PV_EGU="")
add_analog("IonCurrent_He","REAL" ,                     PV_DESC="Helium ion current",                PV_EGU="")
add_analog("IonCurrent_N","REAL" ,                     PV_DESC="Nitrogen ion current",                PV_EGU="")
add_analog("IonCurrent_O2","REAL" ,                     PV_DESC="Oxygen ion current",                PV_EGU="")
add_analog("PPM","REAL" ,                     PV_DESC="PPM impurities",                PV_EGU="PPM")

#Interlock signals
add_digital("PumpInterlock",          PV_DESC="Start Pumps interlock",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("StartPrisProInterlo",  ARCHIVE=True,          PV_DESC="Start Prisma pro interlock",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("GroupInterlock",          PV_DESC="Group Interlock",       PV_ONAM="True",           PV_ZNAM="False")


#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",   PV_ONAM="InhibitManual",  PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",    PV_ONAM="InhibitForce",   PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",       PV_ONAM="InhibitLocking", PV_ZNAM="AllowLocking")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",    PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",  PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableRemoteButton",      PV_DESC="Enable Remote Gas Analyzer Button", PV_ONAM="TRUE",PV_ZNAM="False")

#Feedbacks
add_analog("FB_DelayTime","REAL" ,              PV_DESC="FB Delay Measuring Time",                   PV_EGU="min")
add_analog("FB_MeasTime","REAL" ,               PV_DESC="FB Measuring Time Intervall",               PV_EGU="min")

#OPI timeouts
add_time("DP_StartingTime",               PV_DESC="Starting Time")
add_time("DP_StoppingTime",               PV_DESC="Stopping Time")
add_time("TP_StartingTime",               PV_DESC="Starting Time")
add_time("TP_StoppingTime",               PV_DESC="Stopping Time")
add_time("Gas_RemoteTime",		  PV_DESC="Activated Time")

#Alarm signals
add_digital("LatchAlarm",                            PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")

add_major_alarm("SSTriggered","SSTriggered",                            PV_ZNAM="NominalState")
add_major_alarm("DP_Starting_TimeOut",    "Starting Time Out",             PV_ZNAM="NominalState")
add_major_alarm("DP_Stopping_TimeOut",    "Stopping Time Out",             PV_ZNAM="NominalState")
add_major_alarm("TP_Starting_TimeOut",    "Starting Time Out",             PV_ZNAM="NominalState")
add_major_alarm("TP_Stopping_TimeOut",    "Stopping Time Out",             PV_ZNAM="NominalState")
add_major_alarm("RemoteGasTimeOut",	  "Remote Gas Time Out",	   PV_ZNAM="NominalState")
add_major_alarm("PPM_Impurities",    "PPM impurities too high",             PV_ZNAM="NominalState")
add_major_alarm("HTTP_Com_Lost",    "HTTP communication lost",             PV_ZNAM="NominalState")
add_major_alarm("Recipe_Fail",	    "Recipe Download fail",		   PV_ZNAM="NominalState")

#OPI timeouts

############################
#  COMMAND BLOCK
############################
define_command_block()


#OPI buttons
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")
add_digital("Cmd_ManuStart_Vac",           PV_DESC="CMD: Manual Start Pumps")
add_digital("Cmd_ManuStop_Vac",            PV_DESC="CMD: Manual Stop Pumps")
add_digital("Cmd_ManuStart_Prisma",           PV_DESC="CMD: Manual Start")
add_digital("Cmd_ManuStop_Prisma",            PV_DESC="CMD: Manual Stop")
add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")
add_digital("Cmd_Remote_Button",       PV_DESC="CMD: Remote Disable Button")



############################
#  PARAMETER BLOCK
############################
define_parameter_block()

add_analog("DelayTime","REAL" ,       	    PV_DESC="Delay Measurment Time",            PV_EGU="min")
add_analog("MeasTime","REAL" ,    	    PV_DESC="Measurement Time Intervall",       PV_EGU="min")




